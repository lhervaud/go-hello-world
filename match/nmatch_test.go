package main

import (
    "testing"
)

func TestMatch(t *testing.T) {

  if match("ab", "ab") != 1 {
    t.Error("Match (ab, ab) failed")
  }
}

func TestNmatch(t *testing.T) {

  if nmatch("ababab", "ab*ab") != 3 {
    t.Error("Nmatch (ab, ab*ab) failed")
  }
}
