package main

import (
    "fmt"
    "os"
)

func main() {

    s1 := os.Args[1]
    s2 := os.Args[2]

    fmt.Println(match(s1, s2))
    fmt.Println(nmatch(s1, s2))
}

func match(s1 string, s2 string) (int) {

    //fmt.Println("match : ", s1, s2)
    if(s1 != "" && s2 != "" && s2[0] == '*') {     
        var retour1 = match(s1[1:], s2)
        //if (retour1)
        //    *c += 1;
        var retour2 = 0
        retour2 = match(s1, s2[1:])
        return retour1 + retour2
    }
    if(s1 != "" && s2 != "" && s1[0] == s2[0]) {
        return match(s1[1:], s2[1:])
    }
    if(s1 == "" && s2 == "") {
        //*c += 1;
        return 1
    }
    if(s2 != "" && s2[0] == '*' && s1 == "") {
        return match(s1, s2[1:])
    }
    return 0

}

func nmatch(s1 string, s2 string) (int) {

    var ret, c = nmatch_rec(s1, s2, 0)
    if (ret > 0) {
        c += 1
    } else {
        c = 0
    }
    return c

}

func nmatch_rec(s1 string, s2 string, c int) (int, int) {
  
    //fmt.Println("nmatch : ", s1, s2, c)
    if(s1 != "" && s2 != "" && s2[0] == '*') {     
        var retour1, c = nmatch_rec(s1[1:], s2, c)
        if (retour1 == 1) {
            c += 1
        }
        var retour2 = 0
        retour2, c = nmatch_rec(s1, s2[1:], c)
        return retour1 + retour2, c
    }
    if(s1 != "" && s2 != "" && s1[0] == s2[0]) {
        return nmatch_rec(s1[1:], s2[1:], c)
    }
    if(s1 == "" && s2 == "") {
        return 1, c
    }
    if(s2 != "" && s2[0] == '*' && s1 == "") {
        return nmatch_rec(s1, s2[1:], c)
    }
    return 0, c

}